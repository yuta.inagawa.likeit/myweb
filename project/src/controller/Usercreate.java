package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class Usercreate
 */
@WebServlet("/Usercreate")
public class Usercreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Usercreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッション

		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			response.sendRedirect("Login");
			return;
		}


		RequestDispatcher dispathcer=request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
		dispathcer.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");
		//パラメーター
		String NewLogin_Id=request.getParameter("login_Id");
		String NewPassword=request.getParameter("password");
		String NewPasswordConf=request.getParameter("passwordConf");
		String NewUsername=request.getParameter("userName");
		String NewBirthdate=request.getParameter("birth_date");
		String NewAddress=request.getParameter("address");

		//dao発動
		UserDao dao=new UserDao();

		//エラー各位
		if(!NewPassword.equals(NewPasswordConf)){
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(NewUsername.equals("")||NewBirthdate.equals("")||NewLogin_Id.equals("")||NewPassword.equals("")||NewPasswordConf.
				equals("")||NewAddress.equals("")) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(dao.findLoginID(NewLogin_Id)) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//メソッド発動
		dao.SignUp(NewLogin_Id,NewUsername,NewPassword,NewAddress,NewBirthdate);
		response.sendRedirect("Userlist");

	}





}
