package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメーターの取得
		String id=request.getParameter("id");
		String loginId =request.getParameter("inputId");
		String password =request.getParameter("password");
		//iインスタンス生成
		UserDao userDao=new UserDao();

		//ユーザービーンズ＝パラメーターとメソッドの引数を合体
		UserDataBeans user=userDao.findByLoginInfo(id,loginId, password);

		//失敗
		if(user==null) {
			request.setAttribute("err", "ログインに失敗しました");

			RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//セッション
		HttpSession session=request.getSession();
		session.setAttribute("user",user);
		//リダイレクト
		response.sendRedirect("Userlist");
	}

}
