package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.AdminDao;

/**
 * Servlet implementation class GoodsDetail
 */
@WebServlet("/GoodsDetail")
public class GoodsDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	if (session.getAttribute("user") == null) {
    		response.sendRedirect("Login");
    		return;
    	}
    	String id = request.getParameter("id");
    	System.out.println(id);

    	AdminDao dao=new AdminDao();
    	ItemDataBeans idb1=dao.getItemInfoById(id);

    	request.setAttribute("idb1", idb1);

		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/goodsDetail.jsp");
		dispatcher.forward(request, response);
	}
}

