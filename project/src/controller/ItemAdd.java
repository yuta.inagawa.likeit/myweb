package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemAdd
 */
@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemAdd() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}


		//選択された商品のIDを型変換し利用
		//item＿idはJSPのうったのを、ゲットパラメーター
		String ID = request.getParameter("item_id");
		//対象のアイテム情報を取得
		ItemDao dao=new ItemDao();
		ItemDataBeans item=dao.getItemByItemID(ID);

		//カートを取得()はキャスト
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();
		}
		//カートに商品を追加。
		cart.add(item);
		//カート情報更新
		session.setAttribute("cart", cart);
		request.setAttribute("cartMsg", "商品を追加しました");

		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

}
