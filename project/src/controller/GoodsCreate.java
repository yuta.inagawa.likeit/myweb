package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.AdminDao;

/**
 * Servlet implementation class GoodsCreate
 */
@WebServlet("/GoodsCreate")
@MultipartConfig(location="C:\\Users\\裕太\\Documents\\myweb\\project\\WebContent\\img", maxFileSize=1048576)
public class GoodsCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	if (session.getAttribute("user") == null) {
    		response.sendRedirect("Login");
    		return;
    	}


    	RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/goodsCreate.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");


        Part part = request.getPart("file");
       /* if(part.getSize()>=0){
    		request.setAttribute("errmsg", "入力された内容は正しくありません");

    		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/goodsCreate.jsp");
    		dispatcher.forward(request, response);
    		return;
            }*/
		String goodsName=request.getParameter("goodsname");
		String categoryId=request.getParameter("categoryname");
		String playerId=request.getParameter("playername");
		String detail=request.getParameter("detail");
		String price =request.getParameter("price");


		if(goodsName.equals("")||categoryId.equals("")||detail.equals("")||price.equals("")) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/goodsCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		String filename = getFileName(part);
		part.write(filename);

		AdminDao dao=new AdminDao();
		dao.insertItem(goodsName, categoryId, playerId, detail, filename, price);

		response.sendRedirect("Admin");

	}

    private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
