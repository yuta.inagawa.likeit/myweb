package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;
import dao.ItemDao;

/**
 * Servlet implementation class BuyComplete
 */
@WebServlet("/BuyComplete")
public class BuyComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyComplete() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}
		//メソッド、セッションを消した情報をカートに入れる。ここで切らないと永久的に続く。
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) MywebHelper.removeSession(session,"cart");
		//同様にBDBも消して、情報を入れる。
		BuyDataBeans bdb = (BuyDataBeans) MywebHelper.removeSession(session, "bdb");

		//登録分インスタンス。
		BuyDao dao = new BuyDao();
		BuyDetailDao dao1 =new BuyDetailDao();
		//セッションの中のBDBがDAOのインサート文の中に入る。
		//INT　BUYIDの受けを持たせ、インサート自体をID化。
		int buyId=dao.insertBuyData(bdb);

		//拡張FOR分回して、
		//購入詳細情報を購入情報IDに紐づけて登録。
		for(ItemDataBeans cartData:cart) {
			BuyDetailDataBeans bddb=new BuyDetailDataBeans();
			bddb.setBuy_id(buyId);
			bddb.setItem_id(cartData.getId());
			dao1.InsertBuyDetail(bddb);
		}

		//getIDでそれぞれをとる。
		//購入画面何を買ったか！
		BuyDataBeans resultBuyData= dao.findByBuyId(buyId) ;
		request.setAttribute("result",resultBuyData);

		//アイテム何買ったか
		ArrayList<ItemDataBeans> resultBuyItemData=dao1.getItemByBuyID(buyId);
		request.setAttribute("result1",resultBuyItemData);

		ItemDao dao2=new ItemDao();
		ArrayList<ItemDataBeans> itemlist=dao2.getRandItem(3);

		request.setAttribute("itemlist",itemlist);

		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/buyComplete.jsp");
		dispatcher.forward(request, response);


		}



}
