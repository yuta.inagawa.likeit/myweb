package controller;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * Servlet implementation class MywebHelper
 */
@WebServlet("/MywebHelper")
public class MywebHelper{
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getPrice();
		}
		return total;
	}
	public static int getTotalItemCount(ArrayList<ItemDataBeans> items) {

		return items.size();
	}
	//カートセッションリムーブ。
	public static Object removeSession(HttpSession session,String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}
}
