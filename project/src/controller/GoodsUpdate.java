package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.ItemDataBeans;
import dao.AdminDao;

/**
 * Servlet implementation class GoodsUpdate
 */
@WebServlet("/GoodsUpdate")
@MultipartConfig(location="C:\\Users\\裕太\\Documents\\myweb\\project\\WebContent\\img", maxFileSize=1048576)
public class GoodsUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	if (session.getAttribute("user") == null) {
    		response.sendRedirect("Login");
    		return;
    	}
    	String id = request.getParameter("id");
    	System.out.println(id);

    	AdminDao dao=new AdminDao();
    	ItemDataBeans idb1=dao.getItemInfoById(id);

    	request.setAttribute("idb1", idb1);




    	RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/goodsUpdate.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		String id=request.getParameter("id");
		System.out.println(id);

		Part part = request.getPart("file");
		String goodsName=request.getParameter("goodsname");
		String categoryId=request.getParameter("categoryname");
		String playerName=request.getParameter("player_id");
		String detail=request.getParameter("detail");
		String price =request.getParameter("price");


		if(goodsName.equals("")||categoryId.equals("")||detail.equals("")||price.equals("")) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/goodsUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		String filename = getFileName(part);
		part.write(filename);

		AdminDao dao=new AdminDao();
		dao.updateItem(id, goodsName, categoryId, playerName, detail, filename, price);

		response.sendRedirect("Admin");


	}
	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
