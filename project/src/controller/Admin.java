package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.PlayerDataBeans;
import dao.AdminDao;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/Admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}

		AdminDao dao=new AdminDao();
		ArrayList<PlayerDataBeans> pdb=dao.selectPlayerId();

		request.setAttribute("pdb", pdb);


		ArrayList<ItemDataBeans> IDB = dao.getItemData();

		request.setAttribute("idb", IDB);

		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/admin.jsp");
		dispatcher.forward(request, response);



	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメーター→DAO→サーブレット
		request.setCharacterEncoding("UTF-8");
		String searchName=request.getParameter("name");
		String category_id=request.getParameter("category_id");
		String fromcreatedate=request.getParameter("fromcreatedate");
		String tocreatedate=request.getParameter("tocreatedate");

		AdminDao dao=new AdminDao();
		ArrayList<ItemDataBeans> list=dao.searchItem(searchName, category_id, fromcreatedate, tocreatedate);

		request.setAttribute("idb", list);

		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/admin.jsp");
		dispatcher.forward(request, response);



	}

}
