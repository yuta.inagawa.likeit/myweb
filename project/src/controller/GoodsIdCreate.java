package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PlayerDataBeans;
import dao.AdminDao;

/**
 * Servlet implementation class GoodsIdCreate
 */
@WebServlet("/GoodsIdCreate")
public class GoodsIdCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoodsIdCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}
		AdminDao dao=new AdminDao();
		ArrayList<PlayerDataBeans> pdb=dao.selectPlayerId();

		session.setAttribute("pdb", pdb);
		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/goodsIdCreate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String newName=request.getParameter("name");

		if(newName.equals("")) {
			request.setAttribute("err", "入力内容が間違っています");
			RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/goodsIdCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		AdminDao dao=new AdminDao();
		dao.insertPlayerId(newName);
		response.sendRedirect("Admin");
	}

}
