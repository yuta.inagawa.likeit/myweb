package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class Userlist
 */
@WebServlet("/Userlist")
public class Userlist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Userlist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			response.sendRedirect("Login");
			return;
		}
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//だお発動
		UserDao udb=new UserDao();

		ArrayList<UserDataBeans> list= udb.findAll();

		request.setAttribute("list", list);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメーター→DAO→サーブレット
		request.setCharacterEncoding("UTF-8");

		String searchId=request.getParameter("inputuserID");
		String user_Name=request.getParameter("inputusername");
		String fromBirthdate=request.getParameter("frombirthdate");
		String toBirthdate=request.getParameter("tobirthdate");

		UserDao dao=new UserDao();
		//DAOを発動しその中身とリスト化
		ArrayList<UserDataBeans> list=dao.search(searchId, user_Name, fromBirthdate, toBirthdate);

		//セットアトリビュート
		request.setAttribute("list", list);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);
	}

}
