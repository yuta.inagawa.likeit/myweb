package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.PresentDataBeans;
import beans.UserDataBeans;
import dao.PresentDao;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}
		//インスタンス
		PresentDao dao=new PresentDao();
		int presentId=Integer.parseInt(request.getParameter("present_id"));
		PresentDataBeans userSelect=dao.getPresentId(presentId);
		//プレゼントID取得
		//買い物かご
		ArrayList<ItemDataBeans> cartList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
	    //合計点数、総合計、bdbはbuyターブルに直結するため、その情報をここでセットする。
		int totalPrice = MywebHelper.getTotalItemPrice(cartList);
		int totalCount = MywebHelper.getTotalItemCount(cartList);

		UserDataBeans user=(UserDataBeans)session.getAttribute("user");


		BuyDataBeans bdb = new BuyDataBeans();
		bdb.setUser_id(user.getId());
		bdb.setTotalprice(totalPrice);
		bdb.setTotalcount(totalCount);
		bdb.setPresent_id(userSelect.getId());
		bdb.setPresent_id_name(userSelect.getId_name());
		bdb.setPresent_price(userSelect.getPrice());

		session.setAttribute("bdb", bdb);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/buyComfirm.jsp");
		dispatcher.forward(request, response);
	}

}
