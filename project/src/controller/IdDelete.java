package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PlayerDataBeans;
import dao.AdminDao;

/**
 * Servlet implementation class IdDelete
 */
@WebServlet("/IdDelete")
public class IdDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IdDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}
		String id = request.getParameter("id");
    	System.out.println(id);

    	AdminDao dao=new AdminDao();
    	PlayerDataBeans pdb1= dao.getPlayerInfoById(id);

    	request.setAttribute("pdb1", pdb1);

		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/idDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id=request.getParameter("id");

		AdminDao dao=new AdminDao();
		dao.deletePlayerID(id);

		response.sendRedirect("Admin");
	}

}
