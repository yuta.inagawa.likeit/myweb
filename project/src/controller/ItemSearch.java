package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemSearch
 */
@WebServlet("/ItemSearch")
public class ItemSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemSearch() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			response.sendRedirect("Login");
			return;
		}

		// 検索ワードでの検索処理

		request.setCharacterEncoding("UTF-8");
		//パラメーター



		//だお発動

		String categoryId = request.getParameter("category_id");
		String playerId = request.getParameter("player_id");
		String searchWord = request.getParameter("searchword");
		String sort = request.getParameter("sort");
		// カテゴリー及び選手名での検索

		// カテゴリIDが空でなく、選手IDが空の場合
		// カテゴリー検索を実行[

		ItemDao dao = new ItemDao();
		ArrayList<ItemDataBeans> list = new ArrayList<>();

		if(categoryId  != null && playerId == null && searchWord== null) {
			list = dao.searchCategory(categoryId, sort);
			request.setAttribute("searchParam", "category_id=" + categoryId);

		} else if(playerId != null && categoryId == null&& searchWord== null) {
			list=dao.searchPlayer(playerId,sort);
			request.setAttribute("searchParam", "player_id=" +playerId);

		}else if(searchWord!=null && playerId==null&& categoryId==null ) {
			list=dao.searchItem(searchWord,sort);
			request.setAttribute("searchParam", "searchword=" +searchWord);
		}

		request.setAttribute("list",list);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/itemsearch.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


	}

}
