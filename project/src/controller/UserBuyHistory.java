package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;

/**
 * Servlet implementation class UserBuyHistory
 */
@WebServlet("/UserBuyHistory")
public class UserBuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistory() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
			}

		String id=request.getParameter("buy_id");
		System.out.println(id);

		BuyDao dao =new BuyDao();
		BuyDetailDao dao1=new BuyDetailDao();

		BuyDataBeans buyHistory =dao.buyHistoryBybuyId(id);
		request.setAttribute("buy", buyHistory);

		ArrayList<ItemDataBeans> itemHistory=dao1.getItemByBuyID2(id);
	    request.setAttribute("item", itemHistory);

		RequestDispatcher dispatcher= request.getRequestDispatcher("WEB-INF/jsp/userBuyHistory.jsp");
		dispatcher.forward(request, response);
	}
}
