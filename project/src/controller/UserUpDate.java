package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserUpDate
 */
@WebServlet("/UserUpDate")
public class UserUpDate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpDate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}
		String id=request.getParameter("id");
		System.out.println(id);

		//初期値
		UserDao userdao=new UserDao();
		UserDataBeans udb2=userdao.findIdDetail(id);

		request.setAttribute("udb2",udb2);

		RequestDispatcher Dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
		Dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");
		//パラメーター
		String id=request.getParameter("id");
		String password=request.getParameter("inputpassword1");
		String passwordconf=request.getParameter("inputpassword2");
		String user_name=request.getParameter("inputusername");
		String birthdate=request.getParameter("inputbrithdate");
		String address=request.getParameter("address");

		//DAO作り。
		UserDao dao=new UserDao();

		if(!password.equals(passwordconf)) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");

			UserDao userdao=new UserDao();
			UserDataBeans udb2=userdao.findIdDetail(id);

			request.setAttribute("udb2", udb2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(user_name.equals("")||birthdate.equals("")||address.equals("")||password.equals("")||passwordconf.equals("")) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");

			UserDao userdao=new UserDao();
			UserDataBeans udb2=userdao.findIdDetail(id);

			request.setAttribute("udb2", udb2);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

	  dao.Update(id, password, user_name, birthdate, address);
	  response.sendRedirect("Userlist");
	}

}
