package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDelete() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user")==null) {
			response.sendRedirect("Login");
			return;
		}
		//配列リクエストパラメーターのネームをVALUESというものをつかう。
		//「getParameter」メソッドでは「値」の中の最初のものだけを取得することができます。
		//全ての「値」を取得するには「getParameterValues」メソッドを使います。
		String[] deleteItemIdList = request.getParameterValues("delete_item_id_list");
		//カート内取得。
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		//初期化
		String cartActionMessage = "";
		//もしリストがNULLじゃなかったら（あったら）
		if (deleteItemIdList != null) {
			//削除対象の商品を削除
			//拡張FOR文 削除リストがある限り＋カートの中のものをID＝deleteIDがなったとき＋カート取り除くメソッド。
			for (String deleteItemId : deleteItemIdList) {
				for (ItemDataBeans cartInItem : cart) {
					if (cartInItem.getId() == Integer.parseInt(deleteItemId)) {
						cart.remove(cartInItem);
						break;
					}
				}
			}
			cartActionMessage = "削除しました";
		} else {
			cartActionMessage = "削除する商品が選択されていません";
		}
		request.setAttribute("cartActionMessage", cartActionMessage);
		RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

}
