package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import com.mysql.jdbc.Connection;

import base.DBManager;
import beans.BuyDataBeans;

/**
 * Servlet implementation class BuyDao
 */
@WebServlet("/BuyDao")
public class BuyDao {

	public int insertBuyData(BuyDataBeans bdb) {
		Connection conn = null;
		PreparedStatement ps = null;
		int autoIncKey = -1;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			ps = conn.prepareStatement("INSERT INTO buy (user_id, totalprice ,create_date) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			;

			ps.setString(1, bdb.getUser_id());
			ps.setInt(2, bdb.getTotalprice());
			ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return autoIncKey;

	}

	//IDによる購入履歴閲覧
	public BuyDataBeans findByBuyId(int buyId) {
		Connection conn = null;
		PreparedStatement ps = null;
		BuyDataBeans bdb = new BuyDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM buy where id=?";

			ps = conn.prepareStatement(sql);
			ps.setInt(1, buyId);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setUser_id(rs.getString("user_id"));
				bdb.setTotalprice(rs.getInt("totalprice"));
				bdb.setCreate_date(rs.getTimestamp("create_date"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return bdb;

	}

	public ArrayList<BuyDataBeans> buyHistoryByUserId(String UserId){
		Connection conn = null;
		PreparedStatement ps=null;
		ArrayList<BuyDataBeans> list=new ArrayList<>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="SELECT * FROM buy where user_id=? order by create_date DESC " ;

			ps=conn.prepareStatement(sql);
			ps.setString(1, UserId);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				BuyDataBeans bdb=new BuyDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setTotalprice(rs.getInt("totalprice"));
				bdb.setCreate_date(rs.getTimestamp("create_date"));
				list.add(bdb);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}

	public BuyDataBeans buyHistoryBybuyId(String id){
		Connection conn = null;
		PreparedStatement ps=null;
		BuyDataBeans bdb=new BuyDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="SELECT * FROM buy where id=?";

			ps=conn.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {

				bdb.setId(rs.getInt("id"));
				bdb.setTotalprice(rs.getInt("totalprice"));
				bdb.setCreate_date(rs.getTimestamp("create_date"));
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return bdb;


	}


}