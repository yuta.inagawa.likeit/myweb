package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import com.mysql.jdbc.Connection;

import base.DBManager;
import beans.ItemDataBeans;
import beans.PlayerDataBeans;

/**
 * Servlet implementation class AdminDao
 */
@WebServlet("/AdminDao")
public class AdminDao {

	public ArrayList<ItemDataBeans> getItemData() {
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく　オーダー句はランダムにSQLから取り出す。
			String sql = "SELECT * FROM item ";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setPrice(rs.getInt("price"));
				idb.setFilename(rs.getString("filename"));
				idb.setCreatedate(rs.getString("createdate"));

				list.add(idb);

			}
			System.out.println("getAllItem completed");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return list;

	}

	//詳細
	public ItemDataBeans getItemInfoById(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		ItemDataBeans idb = new ItemDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "Select * from item where id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setPlayer_id(rs.getString("player_id"));
				idb.setPrice(rs.getInt("price"));
				idb.setFilename(rs.getString("filename"));
				idb.setCreatedate(rs.getString("createdate"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return idb;

	}

	public void deleteItem(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "DELETE FROM item WHERE id=?";

			ps = conn.prepareStatement(sql);
			ps.setString(1, id);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<ItemDataBeans> searchItem(String name, String category_id, String fromcreatedate,
			String tocreatedate) {
		Connection conn = null;

		//コレクション配列の形
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			Statement stmt = conn.createStatement();
			// TODO ここに処理を書いていく
			String sql = "SELECT * From item where id <> 0 ";
			//SQL ＋=

			if (!name.equals("")) {
				sql += "AND name like '%" + name + "%' ";
			}

			if (!category_id.equals("")) {
				sql += "AND category_id = '" + category_id + "' ";
			}
			if (!fromcreatedate.equals("")) {
				sql += "AND createdate >='" + fromcreatedate + "' ";
			}
			if (!tocreatedate.equals("")) {
				sql += "AND createdate <='" + tocreatedate + "' ";
			}
			//結果実行
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// インスタンスに設定し、ArrayListインスタンスに追加
			//rsの結果の中が続く限り、

			while (rs.next()) {
				ItemDataBeans idbs = new ItemDataBeans();
				idbs.setName(rs.getString("name"));
				idbs.setCategory_id(rs.getString("category_id"));
				idbs.setDetail(rs.getString("detail"));
				idbs.setCreatedate(rs.getString("createdate"));
				idbs.setId(rs.getInt("id"));
				idbs.setPrice(rs.getInt("price"));

				list.add(idbs);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}

	//グッツインサート文
	@SuppressWarnings("null")
	public void insertItem(String name, String category_id, String player_id, String detail, String filename,
			String price) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "INSERT INTO item (name,category_id,player_id,detail,filename,price,createdate) VALUES(?,?,?,?,?,?,NOW())";
			ps = conn.prepareStatement(sql);

			ps.setString(1, name);
			ps.setString(2, category_id);
			ps.setInt(3, player_id.equals("") ? 0 : Integer.valueOf(player_id));
			ps.setString(4, detail);
			ps.setString(5, filename);
			ps.setString(6, price);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	//グッツアップデート文
	public void updateItem(String id, String name, String category_id, String player_id, String detail, String filename,
			String price) {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "UPDATE item SET name=?,category_id=?,player_id=?,detail=?,filename=?, price=? Where id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, category_id);
			ps.setInt(3, player_id.equals("") ? 0 : Integer.valueOf(player_id));
			ps.setString(4, detail);
			ps.setString(5, filename);
			ps.setString(6, price);
			ps.setString(7, id);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	public ArrayList<PlayerDataBeans> selectPlayerId(){
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<PlayerDataBeans> list=new ArrayList<PlayerDataBeans>();
		try {
			conn = DBManager.getConnection();
			String sql="SELECT * from player_id";
			ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				PlayerDataBeans pdb=new PlayerDataBeans();
				pdb.setId(rs.getString("id"));
				pdb.setName(rs.getString("playername"));
				list.add(pdb);

			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	//インサート選手
	public void insertPlayerId(String name) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			String sql="insert into player_id (playername) values (?)";

			ps=conn.prepareStatement(sql);

			ps.setString(1, name);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	public PlayerDataBeans getPlayerInfoById(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		PlayerDataBeans pdb = new PlayerDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "Select * from player_id where id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				pdb.setId(rs.getString("id"));
				pdb.setName(rs.getString("playername"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return pdb;
	}
	public void deletePlayerID(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql="DELETE FROM player_id where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	public void updatePlayer(String id,String name) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="UPDATE player_id SET playername=? WHERE id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, id);
			ps.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
