package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import com.mysql.jdbc.Connection;

import base.DBManager;
import beans.ItemDataBeans;

/**
 * Servlet implementation class ItemDao
 */
@WebServlet("/ItemDao")
public class ItemDao {

	public ArrayList<ItemDataBeans> getRandItem(int limit){
		Connection conn = null;
		PreparedStatement ps=null;
		ArrayList <ItemDataBeans> list= new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく　オーダー句はランダムにSQLから取り出す。
			String sql="SELECT * FROM item ORDER BY RAND() LIMIT ?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, limit);
			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setPrice(rs.getInt("price"));
				idb.setFilename(rs.getString("filename"));

				list.add(idb);

			}
			System.out.println("getAllItem completed");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return list;

	}
	//ワード検索DAO
	public ArrayList<ItemDataBeans> searchItem(String searchWord, String sort){
		Connection conn = null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			String sql = null;
			// TODO ここに処理を書いていく
			//全検索
			if(searchWord.length()==0) {
				 sql= "SELECT * from item ";

			}

			//文字検索 +~+が分からなかったのが痛い %(ワイルドカード)"+文字（searchWord）+"%(ワイルドカード)
			else {
				 sql="SELECT * from item WHERE name like '%" +searchWord+ "%' ";

			}



			if(sort == null) {
				sql+= " ORDER BY id  ";

			} else if(sort.equals("new")) {
				sql="SELECT * from item ORDER BY createdate DESC ";
			}else if(sort.equals("old")) {
				sql="SELECT * from item ORDER BY createdate   ";
			}else if(sort.equals("cheaper")) {
				sql="SELECT * FROM item ORDER BY price";
			}else if(sort.equals("expensive")) {
				sql="SELECT * FROM item ORDER BY price DESC";
			}


			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setFilename(rs.getString("filename"));
				idb.setCreatedate(rs.getString("createdate"));

				list.add(idb);
			}



		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;


	}
	public ArrayList<ItemDataBeans> searchCategory(String categoryId, String sort){
		Connection conn = null;
		PreparedStatement ps=null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="SELECT * from item ";

			if(sort==null) {
				sql+=" WHERE category_id=?";
			}else if(sort.equals("new")) {
				sql+=" WHERE category_id=? ORDER BY createdate DESC ";
			}else if (sort.equals("old")) {
				sql+=" WHERE category_id=? ORDER BY createdate  ";
			}else if(sort.equals("cheaper")) {
				sql+=" WHERE category_id=? ORDER BY price ";
			}else if(sort.equals("expensive")) {
				sql+=" WHERE category_id=? ORDER BY price DESC ";
			}




			ps=conn.prepareStatement(sql);

			ps.setString(1, categoryId);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setFilename(rs.getString("filename"));
				idb.setCreatedate(rs.getString("createdate"));

				list.add(idb);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		;
		return list;
	}
	public ArrayList<ItemDataBeans> searchPlayer(String playerId,String sort){
		Connection conn = null;
		PreparedStatement ps=null;
		ArrayList<ItemDataBeans> list = new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="SELECT * from item  ";
			if(sort==null) {
				sql+=" WHERE player_id=?";
			}else if(sort.equals("new")) {
				sql+=" WHERE player_id=? ORDER BY createdate DESC ";
			}else if (sort.equals("old")) {
				sql+=" WHERE player_id=? ORDER BY createdate  ";
			}else if(sort.equals("cheaper")) {
				sql+=" WHERE player_id=? ORDER BY price ";
			}else if(sort.equals("expensive")) {
				sql+=" WHERE player_id=? ORDER BY price DESC ";
			}



			ps=conn.prepareStatement(sql);

			ps.setString(1, playerId);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setPlayer_id(rs.getString("player_id"));
				idb.setFilename(rs.getString("filename"));
				idb.setCreatedate(rs.getString("createdate"));

				list.add(idb);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		;
		return list;
	}

	public ItemDataBeans findbyIdDetail(String id) {
		Connection conn = null;
		PreparedStatement ps=null;
		ItemDataBeans idb=new ItemDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="Select * from item where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				idb.setDetail(rs.getString("detail"));
				idb.setCategory_id(rs.getString("category_id"));
				idb.setPlayer_id(rs.getString("player_id"));
				idb.setFilename(rs.getString("filename"));
				idb.setCreatedate(rs.getString("createdate"));


			}




		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return idb;

	}
	//商品検索 itemadd用
	public ItemDataBeans getItemByItemID(String id) {
		Connection conn = null;
		PreparedStatement ps=null;
		ItemDataBeans item=new ItemDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql= "SELECT * FROM item where id=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
			 item.setId(rs.getInt("id"));
			 item.setName(rs.getString("name"));
			 item.setDetail(rs.getString("detail"));
			 item.setFilename(rs.getString("filename"));
			 item.setCategory_id(rs.getString("category_id"));
			 item.setCreatedate(rs.getString("createdate"));
			 item.setPlayer_id(rs.getString("player_id"));
			 item.setPrice(rs.getInt("price"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return item;
	}

}
