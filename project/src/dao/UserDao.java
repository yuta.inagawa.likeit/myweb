package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import com.mysql.jdbc.Connection;

import base.DBManager;
import beans.UserDataBeans;

public class UserDao {
	//ログインDAO

	public UserDataBeans findByLoginInfo(String id,String login_id, String loginpass) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from user where login_id=? and loginpass=?";
			//?にはいるのは…
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, loginpassMD5(loginpass));
			//実行
			ResultSet rs = ps.executeQuery();
			//rsが無かったら。null。
			if (!rs.next()) {
				return null;
			}
			//あったらカラムを指定して、データを取り出す。
			String idData=rs.getString("id");
			String loginData = rs.getString("login_id");
			String nameData = rs.getString("user_name");

			return new UserDataBeans(idData,loginData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	//テーブル
	public ArrayList<UserDataBeans> findAll() {
		Connection con = null;
		PreparedStatement ps = null;
		//戻り用
		ArrayList<UserDataBeans> list = new ArrayList<UserDataBeans>();

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//<> admin 以外セレクト
			String sql = "SELECT * FROM user  WHERE login_id <> 'admin' " ;

			ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				UserDataBeans user = new UserDataBeans();
				user.setId(rs.getString("id"));
				user.setLogin_id(rs.getString("login_id"));
				user.setUser_name(rs.getString("user_name"));
				user.setCreatedate(rs.getString("createdate"));
				user.setAddress(rs.getString("address"));
				user.setBirthdate(rs.getString("birthdate"));
				user.setUpdatedate(rs.getTimestamp("updatedate"));

				list.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return list;
	}

	//新規登録DAO

	public void SignUp(String login_id,  String user_name,String loginpass, String address, String birthdate) {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			String sql = "INSERT INTO user"
					+ "(login_id,user_name,loginpass,address,createdate,birthdate,updatedate)"
					+ " VALUES(?,?,?,?,NOW(),?,NOW())";
			ps = conn.prepareStatement(sql);

			ps.setString(1,login_id);
			ps.setString(2, user_name);
			ps.setString(3,loginpassMD5(loginpass));
			ps.setString(4,address);
			ps.setString(5,birthdate);

			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public boolean findLoginID(String login_id) {
		Connection con = null;
		PreparedStatement ps = null;

		boolean registLoginId=false;
		try {
			//DB接続
			con = DBManager.getConnection();

			//集約関数。カウント文。ASはエイリアス。カウントの（）になにを集計したいのかをセット。
			String sql = "SELECT count(login_id) AS count FROM user WHERE login_id=? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			//?に入るやるを指定左から順に。
			stmt.setString(1, login_id);

			//結果実行
			ResultSet rs = stmt.executeQuery();

			//結果取得。このままでは1件でも2件でも0件でも入る
			if (rs.next()) {
				//エイリアスcountが1件の場合。registloginidがtrueとなる。☛データベースに在るということ。
				//0件を否定→=0true
				if (rs.getInt("count") != 0) {
					registLoginId = true;
				}
			}
			return registLoginId;

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;

	}

	public UserDataBeans findIdDetail(String id) {
		Connection conn = null;
		PreparedStatement ps=null;
		UserDataBeans udb=new UserDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="SELECT * FROM user WHERE id=?";

			ps=conn.prepareStatement(sql);

			ps.setString(1,id);

			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				udb.setId(rs.getString("id"));
				udb.setLogin_id(rs.getString("login_id"));
				udb.setLoginpass(rs.getString("loginpass"));
				udb.setUser_name(rs.getString("user_name"));
				udb.setUpdatedate(rs.getDate("updatedate"));
				udb.setBirthdate(rs.getString("birthdate"));
				udb.setCreatedate(rs.getString("createdate"));
				udb.setAddress(rs.getString("address"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return udb;

	}
	public void Update(String id,String loginpass,String user_name,String birthdate,String address) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			PreparedStatement ps=null;
			// TODO ここに処理を書いていく
			String sql="UPDATE user SET loginpass=?,user_name=?,birthdate=?,address=? WHERE id=?";
			ps=conn.prepareStatement(sql);

			ps.setString(1, loginpassMD5(loginpass));
			ps.setString(2, user_name);
			ps.setString(3, birthdate);
			ps.setString(4, address);
			ps.setInt(5,  Integer.parseInt(id));

			int result = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	public void Delete(String id) {
		Connection conn = null;
		PreparedStatement ps =null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql="DELETE FROM user WHERE id=?";


			ps=conn.prepareStatement(sql);
			ps.setString(1,id);

			int result=ps.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String loginpassMD5(String loginpass) {
		//ハッシュを生成したい元の文字列
		String source = loginpass;

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理。try文にしたほうがいい。
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		System.out.println(result);

		return result;
	}

	//検索結果配列になる（複数件取ってくるから）
	public ArrayList<UserDataBeans> search(String login_id,String user_name,String fromBirthDate, String toBirthDate ){
		Connection conn = null;

		//コレクション配列の形
		ArrayList <UserDataBeans> list=new ArrayList<UserDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			Statement stmt = conn.createStatement();
			// TODO ここに処理を書いていく
			String sql= "SELECT * From user where login_id <> 'admin'";
			//SQL ＋＝
			// ログインIDが空でなければ条件追加

			if(!login_id.equals("")) {
				sql+="AND login_id = '"+login_id+"'";
			}
			if(!user_name.equals("")) {
				sql+="AND user_name like '%"+user_name+"%'";
			}
			if(!fromBirthDate.equals("")) {
				sql+="AND birthdate >='"+fromBirthDate+"'";
			}
			if(!toBirthDate.equals("")) {
				sql+="AND birthdate <='"+toBirthDate+"'";
			}
			//結果実行
			ResultSet rs=stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
            // インスタンスに設定し、ArrayListインスタンスに追加
			//rsの結果の中が続く限り、rsの結果をゲットしてuserの中のやつをにセット。

			while(rs.next()) {
				UserDataBeans user=new UserDataBeans();
				user.setId(rs.getString("id"));
				user.setLogin_id(rs.getString("login_id"));
				user.setUser_name(rs.getString("user_name"));
				user.setCreatedate(rs.getString("createdate"));
				user.setBirthdate(rs.getString("birthdate"));
				user.setUpdatedate(rs.getDate("updatedate"));
				user.setLoginpass(rs.getString("loginpass"));

				list.add(user);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}



}
