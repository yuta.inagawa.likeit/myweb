package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import com.mysql.jdbc.Connection;

import base.DBManager;
import beans.PresentDataBeans;

/**
 * Servlet implementation class PresentDao
 */
@WebServlet("/PresentDao")
public class PresentDao {
	public ArrayList<PresentDataBeans> getAllPresentId(){
		Connection conn = null;
		PreparedStatement ps = null;
		ArrayList<PresentDataBeans> list=new ArrayList<>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql= "SELECT * FROM present_id";
			ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
			 PresentDataBeans pdb=new PresentDataBeans();
			 pdb.setId(rs.getInt("id"));
			 pdb.setId_name(rs.getString("id_name"));
			 pdb.setPrice(rs.getInt("price"));

			 list.add(pdb);
			}



		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return list;
	}

	public PresentDataBeans getPresentId(int id) {
		Connection conn = null;
		PreparedStatement ps=null;
		PresentDataBeans pdb=new PresentDataBeans();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			String sql="SELECT * FROM present_id where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {

				 pdb.setId(rs.getInt("id"));
				 pdb.setId_name(rs.getString("id_name"));
				 pdb.setPrice(rs.getInt("price"));
				}

			// TODO ここに処理を書いていく

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return pdb;
	}

}
