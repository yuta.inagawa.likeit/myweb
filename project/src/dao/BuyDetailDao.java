package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import com.mysql.jdbc.Connection;

import base.DBManager;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

/**
 * Servlet implementation class BuyDetailDao
 */
@WebServlet("/BuyDetailDao")
public class BuyDetailDao  {

	//詳細を登録するDAO文。
	public void  InsertBuyDetail(BuyDetailDataBeans bddb) {
		Connection conn = null;
		PreparedStatement ps=null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql= "INSERT INTO buy_detail(buy_id,item_id) VALUES (?,?) ";

			ps=conn.prepareStatement(sql);
			ps.setInt(1, bddb.getBuy_id());
			ps.setInt(2, bddb.getItem_id());


			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public ArrayList<ItemDataBeans> getItemByBuyID(int buyId){
		Connection conn = null;
		PreparedStatement ps=null;
		ArrayList<ItemDataBeans> list=new ArrayList<>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql= "SELECT * FROM buy_detail t1 INNER JOIN item t2 ON t1.item_id = t2.id where buy_id = ?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, buyId);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				list.add(idb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}
	public ArrayList<ItemDataBeans> getItemByBuyID2(String buyId){
		Connection conn = null;
		PreparedStatement ps=null;
		ArrayList<ItemDataBeans> list=new ArrayList<>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql= "SELECT * FROM buy_detail t1 INNER JOIN item t2 ON t1.item_id = t2.id where buy_id = ?";
			ps=conn.prepareStatement(sql);
			ps.setString(1, buyId);

			ResultSet rs=ps.executeQuery();

			while(rs.next()) {
				ItemDataBeans idb=new ItemDataBeans();
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				list.add(idb);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;

	}
}
