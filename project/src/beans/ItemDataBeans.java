package beans;

import java.io.Serializable;


public class ItemDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String category_id;
	private String player_id;
	private String filename;
	private String createdate;
	private int totalcount;


	private String tobirthdate;
	private String frombirthdate;




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getPlayer_id() {
		return player_id;
	}
	public void setPlayer_id(String player_id) {
		this.player_id = player_id;
	}
	public int getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	public String getTobirthdate() {
		return tobirthdate;
	}
	public void setTobirthdate(String tobirthdate) {
		this.tobirthdate = tobirthdate;
	}
	public String getFrombirthdate() {
		return frombirthdate;
	}
	public void setFrombirthdate(String frombirthdate) {
		this.frombirthdate = frombirthdate;
	}


}
