package beans;

import java.io.Serializable;

import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class PresentDataBeans
 */
@WebServlet("/PresentDataBeans")
public class PresentDataBeans implements Serializable {
 private int id;
 private String id_name;
 private int price;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getId_name() {
	return id_name;
}
public void setId_name(String id_name) {
	this.id_name = id_name;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
}
