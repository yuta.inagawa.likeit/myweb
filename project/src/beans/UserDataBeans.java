package beans;

import java.io.Serializable;
import java.util.Date;
//高度な継承　 implements アクセサ、ユーザー関連のカプセル化
public class UserDataBeans implements Serializable{
	private String id;
	private String login_id;
	private String user_name;
	private String loginpass;
	private String createdate;
	private String address;
	private String birthdate;
	private Date updatedate;

	private String tobirthdate;
	private String frombirthdate;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getLoginpass() {
		return loginpass;
	}
	public void setLoginpass(String loginpass) {
		this.loginpass = loginpass;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	//コンストラクタ生成、インスタンスの引数有はコンストラクタを作らなければならない。
	//ログインセッションに必要なやつのコンストラクタ
		public UserDataBeans(String id,String login_Id,String user_name) {
			this.id=id;
			this.login_id = login_Id;
			this.user_name = user_name;
		}


		//新規登録コンストラクタ
		public UserDataBeans(String login_id,String loginpass,String user_name,String address,String createdate,
				String birthdate, Date updatedate) {
			this.login_id=login_id;
			this.loginpass=loginpass;
			this.user_name=user_name;
			this.address=address;
			this.createdate=createdate;
			this.birthdate=birthdate;
			this.updatedate=updatedate;
		}

		//二重ログインIDメソッド用コンストラクタ
		public UserDataBeans(String login_Id) {
			this.login_id = login_Id;
		}
		public UserDataBeans() {
			// TODO 自動生成されたコンストラクター・スタブ
		}
		public String getTobirthdate() {
			return tobirthdate;
		}
		public void setTobirthdate(String tobirthdate) {
			this.tobirthdate = tobirthdate;
		}
		public String getFrombirthdate() {
			return frombirthdate;
		}
		public void setFrombirthdate(String frombirthdate) {
			this.frombirthdate = frombirthdate;
		}






}
