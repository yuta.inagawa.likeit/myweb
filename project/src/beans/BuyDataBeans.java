package beans;

import java.io.Serializable;
import java.util.Date;

import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class BuyDataBeans
 */
@WebServlet("/BuyDataBeans")
public class BuyDataBeans implements Serializable {
	private int id;
	private String user_id;
	private int totalprice;
	private int present_id;
	private Date create_date;
	private String present_id_name;
	private int present_price;
	private int totalcount;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String string) {
		this.user_id = string;
	}
	public int getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(int totalprice) {
		this.totalprice = totalprice;
	}
	public int getPresent_id() {
		return present_id;
	}
	public void setPresent_id(int present_id) {
		this.present_id = present_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public int getPresent_price() {
		return present_price;
	}
	public void setPresent_price(int present_price) {
		this.present_price = present_price;
	}
	public int getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	public String getPresent_id_name() {
		return present_id_name;
	}
	public void setPresent_id_name(String present_id_name) {
		this.present_id_name = present_id_name;
	}


}
