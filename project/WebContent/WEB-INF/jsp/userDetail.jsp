<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>ユーザー詳細</title>
</head>

	<header>
		<div class="p-3 mb-2 bg-dark text-white" role="alert">
		🧺STORE INAGAWA🏀
			<p class="text-right">
				${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			</p>
		</div>
	</header>


<body>
<div class=container>
		<form>
			<h2>
				<p class="text-center">🏀ユーザ情報詳細参照🏀</p>
			</h2>
		</form>
		<form>
			<p class="text-left">ログインID</p><p class="text-center">${udb1.id}</p>

			<br> <br>
			<p class="text-left">ユーザー名</p>
			<p class="text-center">${udb1.user_name}</p>
			<br> <br>
			<p class="text-left">生年月日</p>
			<p class="text-center">
				${udb1.birthdate }<br> <br>
			<p class="text-left">登録日時
			<p class="text-center">${udb1.createdate}</p>
			<br> <br>
			<p class="text-left">更新日時</p>
			<p class="text-center">
				${udb1.updatedate}</p><br>
		</form>
		<form>
			<a href="Userlist" class="badge badge-back"><h4>戻る</h4></a>
		</form>
	</div>


</body>
</html>