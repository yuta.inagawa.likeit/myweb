<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			<a href="Cart"><button type="button" class="btn btn-light">
					<i class="fas fa-shopping-cart">カートへ</i>
				</button></a> <a href="UserData"><button type="button" class="btn btn-light">
					<i class="fas fa-user">ユーザー情報</i>
				</button></a>

			<c:if test="${user.login_id=='admin'}">
				<a href="Admin">
					<button type="button" class="btn btn-light">
						<i class="fas fa-user-secret">管理者画面</i>
					</button>
				</a>
			</c:if>
		</p>
	</div>
</header>
<body>

	<div class="container">
		<br> <br>
		<div class="row justify-content-md-center">
			<h2>商品詳細</h2>
		</div>
		<br>
		<h4>
			<p class="text-center">${idb.name}</p>
		</h4>
		<div class="row justify-content-md-center">
			<div class="col-md-auto">
				<figure class="figure">
					<img src="img/${idb.filename}" alt="画像">
				</figure>
			</div>
		</div>
		<p class="text-center">${idb.detail}</p>
		<p class="text-center">${idb.price}円</p>
		<form action="ItemAdd" method="POST">
			 <div class="row justify-content-md-center">
    <div class="col col-lg-2">
					<input type="hidden" name="item_id" value="${idb.id}">
					<button type="submit" class="btn btn-success"><i class="fas fa-cart-plus">買い物かごに追加!</i></button>
				</div>
				</div>
		</form>
		<br>
		 <div class="row justify-content-md-center">
    <div class="col col-lg-2">
			<a href="Index">
				<button type="button" class="btn btn-warning"><i class="fas fa-home">ホームに戻る</i></button>
			</a>
		</div>
	</div>
	</div>
</body>
</html>