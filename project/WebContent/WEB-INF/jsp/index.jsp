<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<div class="container">
		<div class="p-3 mb-2 bg-success text-center ">
			<h2>STORE INAGAWA EC SHOP</h2>
		</div>
		<h4>
			<p class="text-center">NBA BOSTON CELITICSのグッツはうちで！</p>
		</h4>
		<br> <br> <br>

		<div class="form-group row">
			<label for="search_word" class="col-sm-2 col-form-label"><h4>検索ワード</h4></label>
			<form action="ItemSearch" method="get">
			<div class="col-sm-10">

				 <input type="text" class="form-control" name="searchword" id="searchword">
					<p class="text-right">
						<button type="submit" class="btn btn-outline-dark">検索</button>
					</p>
			</div>
			</form>
		</div>
		<br> <br>
		<div class="row">
			<div class="col">
				<h4>カテゴリ一覧</h4>
			</div>
			<div class="col">
				<a href="ItemSearch?category_id=1">ユニフォーム/uniform</a>
			</div>
			<div class="col">
				<a href="ItemSearch?category_id=3">カジュアルウェア/casual</a>
			</div>
		</div>

		<div class="row">
			<div class="col"></div>
			<div class="col">
				<a href="ItemSearch?category_id=2">帽子・キャップ/cap</a>
			</div>
			<div class="col">
				<a href="ItemSearch?category_id=4">アクセサリー・その他/accessories</a>
			</div>
		</div>
		<br> <br>
		<div class="row">
			<div class="col">
				<h4>選手名</h4>
			</div>
			<div class="col">
				<a href="ItemSearch?player_id=2">ジェイソン・テイタム</a>
			</div>
			<div class="col">
				<a href="ItemSearch?player_id=1">カイリー・アービング</a>
			</div>
			<div class="col">
				<a href="ItemSearch?player_id=3">ジェイレン・ブラウン</a>
				</div>
				</div>
				<div class="row">
					<div class="col"></div>
					<div class="col">
						<a href="ItemSearch?player_id=4">ゴードン・ヘイワード</a>
					</div>
					<div class="col">
						<a href="ItemSearch?player_id=5">アル・フォーフォード</a>
					</div>
		</div>
		<br> <br>
		<h3>
			<p class="text-center">おススメ商品</p>
		</h3>
		<div class="row">
			<c:forEach var="itemlist" items="${itemlist}">
			<div class="col s12 m4">
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="img/${itemlist.filename}">
					<div class="card-body">
						<h5 class="card-title">${itemlist.name}</h5>
						<p class="card-text">${itemlist.price}円</p>
						<a href="ItemDetail?item_id=${itemlist.id}" class="btn btn-primary">詳細</a>
					</div>
				</div>
			</div>
			</c:forEach>
</body>
</html>