<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<div class="container">
		<form action="BuyComplete" method="POST">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">商品名</th>
						<th scope="col">値段</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${cart}">
						<tr>
							<th scope="row">${item.name}</th>
							<td>${item.price}</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>
			<!--<table class="table">
				<thead class="thead-light">
				<th scope="col">プレゼント包装</th>
				<th scope="col"></th>
				<th scope="col">金額</th>
			<tr>
				 ここが怪しい何故表示されないのか？リクエストスコープがないのか？サーブレットへ。
				<td class="center">${bdb.present_id_name}</td>
				<td class="center"></td>
				<td class="center">${bdb.present_price}円</td>
			</tr>
			</thead>
			</table> -->
			<table class="table">
				<thead class="thead-light">
					<tr>
						<th scope="col">総点数</th>
						<th scope="col">合計金額</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">${bdb.totalcount}</th>
						<td>${bdb.totalprice}</td>
					</tr>
				</tbody>
			</table>


			<p class="text-center">
				<button type="submit" class="btn btn-info">購入完了へ</button>
			</p>
		</form>
		<a href="Cart"><p class="text-center">
				<button type="button" class="btn btn-info">カートへ戻る</button>
			</p></a>
	</div>
</body>
</html>