<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<div class="container">
		<div class="row center">
			<h5>カートアイテム</h5>
			<c:if test="${cartActionMessage!=null}">
			<font color="red">☛${cartActionMessage}</font>
			</c:if>
		</div>
		 <form action="BuyConfirm" method="POST">
			<table class="table">
				<thead class="thead-dark">

					<tr>
						<th scope="col">商品名</th>
						<th scope="col">値段</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${cart}">
						<tr>
							<th scope="row">${item.name}</th>
							<td>${item.price}円</td>
						</tr>
					</c:forEach>
					<!--  <tr>
						<td class="center">
						<td class="center">
							<div class="input-field col s8 offset-s2 ">
								<select name="present_id">
									<c:forEach var="pdb" items="${pdb}">
										<option value="${pdb.id}">${pdb.id_name}</option>
									</c:forEach>
								</select> <label>プレゼント包装</label>
							</div>
						</td>
					</tr>
				</tbody>-->


				</tbody>
			</table>


			<p class="text-center">
				<button class="btn btn-info" type="submit">購入確認</button>
			</p></div>
		</form>
</body>
</html>