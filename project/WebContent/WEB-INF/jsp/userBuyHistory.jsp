<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<div class="container">
		<br>
		<table class="table" >
			<thead class="thead-dark">
				<tr>
					<th class="center" style="width: 40%;">購入日時</th>
					<th class="center" style="width: 40%">合計金額</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="center">${buy.create_date}</td>
					<td class="center">${buy.totalprice}</td>
				</tr>
			</tbody>
		</table>
		<br>
		<br>
		<table class="table" >
			<thead class="thead-light">
				<tr>
					<th class="center" style="width: 30%;">商品</th>
					<th class="center"></th>
					<th class="center" style="width: 30%">小計金額</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${item}">
				<tr>

					<td class="center">${item.name}</td>
					<td class="center"></td>
					<td class="center">${item.price}円</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>