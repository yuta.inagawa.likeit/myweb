<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<h2>
		<p class="text-center">購入完了しました‼</p>
	</h2>
	<p class="text-center">
		<a href="Index"><button type="button" class="btn btn-light">ホーム画面に戻る</button></a>
	</p>

	<div class="container">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">購入日時</th>
					<th scope="col">購入金額</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>${result.create_date}</td>
					<td>${result.totalprice}円</td>
				</tr>
			</tbody>
		</table>

	<table class="table">
			<thead class="thead-light">
				<tr>
					<th scope="col">購入商品</th>
					<th scope="col">単価</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="result1" items="${result1}">
				<tr>
					<td>${result1.name}</td>
					<td>${result1.price}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
		<h3>
			<p class="text-center">買い忘れてませんか？おススメ商品</p>
		</h3>
		<div class="row">
			<c:forEach var="itemlist" items="${itemlist}">
			<div class="col s12 m4">
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="img/${itemlist.filename}">
					<div class="card-body">
						<h5 class="card-title">${itemlist.name}</h5>
						<p class="card-text">${itemlist.price}円</p>
						<a href="ItemDetail?item_id=${itemlist.id}" class="btn btn-primary">詳細</a>
					</div>
				</div>
			</div>
			</c:forEach>
	</div>

</body>
</html>