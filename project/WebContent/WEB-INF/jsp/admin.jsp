<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>管理者画面</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			<a href="Cart"><button type="button" class="btn btn-light">カートへ</button></a>
			<a href="UserData"><button type="button" class="btn btn-light">ユーザーデーターへ</button></a>

			<c:if test="${user.login_id=='admin'}">
				<a href="Admin">
					<button type="button" class="btn btn-light">管理者画面</button>
				</a>
			</c:if>
		</p>
	</div>
</header>

<body>
	<div class="container">
		<h1 class=userlist1>
			<p class="text-center">
				<u>グッツ一覧（管理者専用画面）<i class="fas fa-basketball-ball"></i></u>
			</p>
		</h1>

		<p class="text-right">
			<a class="btn btn-outline-dark" href="GoodsCreate">新規登録</a> <br>
			<br>
		<p class="text-right">
			<a class="btn btn-outline-dark" href="GoodsIdCreate">新規選手ID登録</a> <br>
			<br>
		<form action=Admin method="post">
			<div class="form-group row">
				<label for="goos" class="col-sm-2 col-form-label">商品名</label>
				<div class="col-sm-10">
					<input type="userID" class="form-control" name="name" id="name">
				</div>
			</div>
			<div class="form-group row">
				<label for="goos" class="col-sm-2 col-form-label">カテゴリID </label>
				<div class="col-sm-10">
					<input type="userID" class="form-control" name="category_id"
						id="category_id">
				</div>
			</div>


			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">登録日時</label>
				<div class="row col-sm-10">
					<div class="col-sm-5">
						<input type="date" name="fromcreatedate">
					</div>

					<div class="col-sm-1 text-center">～</div>
					<div class="col-sm-5">
						<input type="date" name="tocreatedate">
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-10">
					<p class="text-right">
						<button type="submit" class="btn btn-outline-dark">検索</button>
					</p>
				</div>
			</div>
		</form>

		<br>

		<h3>
			<p class="text-center">選手一覧</p>
		</h3>
		<p class="text-right">
			<a class="btn btn-outline-dark" href="GoodsIdCreate">新規選手ID登録</a> <br>
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th>ID</th>
					<th></th>
					<th>PLAYERNAME</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="pdb" items="${pdb}">
					<tr class="table-light">
						<td>${pdb.id}</td>
						<td></td>
						<td>${pdb.name}</td>
						<td><a class="btn btn-warning" href="IdUpdate?id=${pdb.id}"
							role="button"><i class="fas fa-edit"></i></a>
							<a class="btn btn-danger"
							href="IdDelete?id=${pdb.id}" role="button"><i class="fas fa-trash-alt"></i></a>
				</c:forEach>
			</tbody>
		</table>
		<br>

		<h3><p class=text-center>カテゴリ名</p></h3>
		<table class="table">
		 	<thead class="thead-dark">
				<tr>
					<th scope="col">カテゴリID</th>
					<th scope="col">ID名</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">1</th>
					<td>ユニフォーム</td>
				</tr>
				<tr>
					<th scope="row">2</th>
					<td>帽子</td>
				</tr>
				<tr>
					<th scope="row">3</th>
					<td>カジュアルウェア</td>
				</tr>
				<tr>
					<th scope="row">4</th>
					<td>アクセサリー・その他</td>
				</tr>
			</tbody>
		</table>
		<h3>
			<p class="text-center">商品一覧</p>
		</h3>
		<p class="text-right">
			<a class="btn btn-outline-dark" href="GoodsCreate">新規登録</a> <br>
		<table class="table">
			<thead class="thead-dark" role="alert">
				<tr>
					<th scope="col">商品名</th>
					<th scope="col">カテゴリID</th>
					<th scope="col">登録日時</th>
					<th scope="col">値段
					<th>
					<th scope="col">
					<th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="idb" items="${idb}">
					<tr class="table-light">
						<th scope="row">${idb.name}</th>
						<td>${idb.category_id}</td>
						<td>${idb.createdate}</td>
						<td>${idb.price}円</td>

						<td><a class="btn btn-primary"
							href="GoodsDetail?id=${idb.id}" role="button">詳細</a> <a
							class="btn btn-warning" href="GoodsUpdate?id=${idb.id}"
							role="button">更新</a> <a class="btn btn-danger"
							href="GoodsDelete?id=${idb.id}" role="button">削除</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>