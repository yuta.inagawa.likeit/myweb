<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>ユーザー更新</title>
</head>


	<header>
		<div class="p-3 mb-2 bg-dark text-white" role="alert">
		🧺STORE INAGAWA🏀
			<p class="text-right">
				${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			</p>
		</div>
	</header>

<body>
<div class=container>
		<form>
			<h2>
				<p class="text-center">🏀ユーザー情報更新🧺</p>
			</h2>
		</form>

		<form action="UserUpDate" method="post">
			<p class="text-left">ログインID</p>

			<p class="text-center">${udb2.login_id}</p>
			<br>

			<p class="text-left">パスワード</p>
			<p class=text-center>
				<input type="password" name="inputpassword1" >
			<p class="text-left">パスワード（確認）</p>
			<p class=text-center>
				<input type="password" name="inputpassword2" >
			<p class="text-left">ユーザー名</p>
			<p class=text-center>
				<input type="text" name="inputusername" value="${udb2.user_name}">
			<p class="text-left">生年月日</p>
			<p class=text-center>
				<input type="date" name="inputbrithdate" value="${udb2.birthdate}"> <br>
				<br>
			<p class="text-left">住所</p>
			<p class=text-center>
				<input type="text" name="address" value="${udb2.address}"> <br>
				<br>
				<button type="submit" class="btn btn-link">
					<h3>更新</h3>
				</button>
				<!--hiddenは画面上には表示しないでサーブレットに値を渡したい場合に利用-->
				<input type="hidden" name="id" value="${udb2.id}">
		</form>

		<form>
			<a href="Userlist" class="badge badge-back"><h4>戻る</h4></a>
		</form>
	</div>

</body>
</html>