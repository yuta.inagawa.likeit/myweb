<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>ユーザー新規登録画面</title>
</head>


<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		🧺STORE INAGAWA🏀
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
		</p>
	</div>
</header>


<body>
	<h2>
		<p class="text-center">🏀ユーザー新規登録🏀</p>
	</h2>
	<br>
	<br>
	<c:if test="${errmsg != null}">
		<p class="text-danger">${errmsg}</p>
	</c:if>
	<div class="container">
		<form method="post" action="Usercreate">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="login_Id"
						id="loginId">
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="password"
						id="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" name="passwordConf"
						id="passwordConf">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="userName"
						id="userName">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" class="form-control" name="birth_date"
						id="birth_date">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">住所</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="address" id="address">
				</div>
			</div>

			<div class="submit-button-area">
				<button type="submit" class="btn btn-dark btn-lg btn-block">登録</button>
			</div>
		</form>
		<div class="col-xs-4">
			<a href="Userlist">戻る</a>
		</div>

	</div>
</body>
</html>