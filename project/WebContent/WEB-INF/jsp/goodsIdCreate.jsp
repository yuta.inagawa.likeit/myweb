<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>管理者画面</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			<a href="Cart"><button type="button" class="btn btn-light">カートへ</button></a>
			<a href="UserData"><button type="button" class="btn btn-light">ユーザーデーターへ</button></a>

			<c:if test="${user.login_id=='admin'}">
				<a href="Admin">
					<button type="button" class="btn btn-light">管理者画面</button>
				</a>
			</c:if>
		</p>
	</div>
</header>
<body>
	<div class="container">
	<c:if test="${err!=null}" >
			<font color="red">${err}</font>
			</c:if>

	現在のプレイヤー一覧
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th>ID</th>
					<th></th>
					<th>PLAYERNAME</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="pdb" items="${pdb}">
					<tr class="table-light">
						<td>${pdb.id}</td>
						<td></td>
						<td>${pdb.name}</td>
				</c:forEach>
			</tbody>
			</table>
			<br>


			<form action="GoodsIdCreate" method="post">
			<input class="form-control" type="text" name="name" placeholder="選手名入れてください、IDは勝手につきます。">
			<div class=row>
			<div class="center-block">
			<button type="submit" class="btn btn-dark">プレイヤーID登録</button>
	        </div>
	        </div>
			</form>
</div>
</body>
</html>