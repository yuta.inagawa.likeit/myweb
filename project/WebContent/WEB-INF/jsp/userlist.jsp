<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>ユーザー一覧</title>
</head>

<body>
	<header>
		<div class="p-3 mb-2 bg-dark text-white" role="alert">
		🧺STORE INAGAWA🏀
			<p class="text-right">
				${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			</p>
		</div>
	</header>
	<div class="container">
		<h1 class=userlist1><p class="text-center"><u>ユーザー一覧</u></p></h1>
		<p class="text-left"><a class="btn btn-success text-white"  href="Index">ECサイトへ</a></p>

		<p class="text-right">
			<a class="btn btn-outline-dark" href="Usercreate">新規登録</a> <br>
			<br>

		<form action="Userlist" method="post">
			<div class="form-group row">
				<label for="inputuserID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="userID" class="form-control" name="inputuserID" id="inputuserID">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputusername" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="username" class="form-control" name="inputusername" id="inputusername">
				</div>
			</div>
			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="row col-sm-10">
					<div class="col-sm-5">
						<input type="date" name="frombirthdate">
					</div>

					<div class="col-sm-1 text-center">～</div>
					<div class="col-sm-5">
						<input type="date" name="tobirthdate">
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-10">
					<p class="text-right">
						<button type="submit" class="btn btn-outline-dark">検索</button>
					</p>
				</div>
			</div>
		</form>



		<table class="table">
			<thead class="thead-dark" role="alert">
				<tr>
					<th scope="col">ログインID</th>
					<th scope="col">ユーザー名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="list" items="${list}">

				<tr class="table-light">
					<th scope="row">${list.login_id}</th>
					<td>${list.user_name}</td>
					<td>${list.birthdate}</td>

					<td><a class="btn btn-primary" href="UserDetail?id=${list.id}"role="button">詳細</a>


					<c:if test="${user.login_id=='admin'||user.login_id==list.login_id}">

					<a class="btn btn-warning" href="UserUpDate?id=${list.id}" role="button">更新</a> </c:if>

					 <c:if test="${user.login_id=='admin'}">

					<a class="btn btn-danger" href="UserDelete?id=${list.id}" role="button">削除</a></c:if>

					</td>
				</tr>
				</c:forEach>


				<!--
				<tr class="table-light">
					<th scope="row">ID0002</th>
					<td>サンプル１</td>
					<td>2001年11月12日</td>
					<td><a class="btn btn-primary" href="userDetail.html"
						role="button">詳細</a> <a class="btn btn-warning"
						href="userUpdate.html" role="button">更新</a> <a
						class="btn btn-danger" href="userDelete.html" role="button">削除</a></td>
				</tr>
				<tr class="table-light">
					<th scope="row">ID0003</th>
					<td>サンプル2</td>
					<td>2000年01月01日</td>
					<td><a class="btn btn-primary" href="userDetail.html"
						role="button">詳細</a> <a class="btn btn-warning"
						href="userUpdate.html" role="button">更新</a> <a
						class="btn btn-danger" href="userDelete.html" role="button">削除</a></td>
						-->


			</tbody>
		</table>
	</div>

</body>
</html>