<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a>
			<a href="Cart"><button type="button" class="btn btn-light">
					<i class="fas fa-shopping-cart">カートへ</i>
				</button></a> <a href="UserData"><button type="button" class="btn btn-light">
					<i class="fas fa-user">ユーザー情報</i>
				</button></a>

			<c:if test="${user.login_id=='admin'}">
				<a href="Admin">
					<button type="button" class="btn btn-light">
						<i class="fas fa-user-secret">管理者画面</i>
					</button>
				</a>
			</c:if>
		</p>
	</div>
</header>
<body>
	<div class=container>
		<form>
			<h2>
				<p class="text-center">🏀プレイヤー更新🧺</p>
			</h2>
		</form>
		<c:if test="${errmsg!= null}">
			<font color="red">${errmsg}</font>
		</c:if>
		<form action=IdUpdate method="post">
			<p class="text-center">ID:${pdb2.id}</p>
			<br>
			<p class=text-center>
				プレイヤー名 <input type="text" name="playername" value="${pdb2.name}">
				<input type="hidden" name="id" value="${pdb2.id}"> <br>
				<button type="submit" class="btn btn-link">
					<h3>更新</h3>
				</button>
		</form>


		<h4>
			<p class="text-center">
				<a href="Admin" class="badge badge-back">戻る</a>
			</p>
		</h4>

	</div>
</body>
</html>