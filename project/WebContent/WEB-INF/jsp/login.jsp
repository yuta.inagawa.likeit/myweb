<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面/STORE INAGAWA</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>


<body>
<div class="container">
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="login.html">STORE
				🧺INAGAWA Log in🏀</a>
		</nav>
	</header>
	<br>
	<br>
	<br>
    <c:if test="${err!= null}" >
		<font color="red">${err}</font>
	</c:if>

	<c:if test="${out!=null}">
	    <font color="green">${out}</font>
	</c:if>

		<form action="Login" method=post>

			<div class="form-group">
				<label for="exampleInputEmail1">Login_ID</label> <input type="text"
					class="form-control" id="inputId" name=inputId
					placeholder="IDを入力してください">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label> <input
					type="password" class="form-control" name="password" id="password"
					placeholder="passwordを入力してください">
			</div>
			<input type="hidden" value="id" name="id">
			<button class="btn btn-dark" type="submit">ログイン</button>
		</form>

	</div>
</body>
</html>