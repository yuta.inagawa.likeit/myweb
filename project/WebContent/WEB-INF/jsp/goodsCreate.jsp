<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>

	<h2>
		<p class="text-center">グッツ新規登録🏀</p>
	</h2>
	<br>
	<br>

	<div class="container">
		<c:if test="${errmsg != null}">
		<p class="text-danger">${errmsg}</p>
	</c:if>
		<form method="post" action="GoodsCreate" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group row">
				<label for="goodsname" class="col-sm-2 col-form-label">グッツ商品名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="goodsname"
						id="goodsname">
				</div>
			</div>

			<div class="form-group row">
				<label for="categoryname" class="col-sm-2 col-form-label">カテゴリID</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="categoryname"
						id="categoryname">
				</div>
			</div>

			<div class="form-group row">
				<label for="playername" class="col-sm-2 col-form-label">選手ID</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="playername"
						id="playername">
				</div>
			</div>

			<div class="form-group row">
				<label for="detail" class="col-sm-2 col-form-label">詳細</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="detail" id="detail">
				</div>
			</div>

			<div class="form-group row">
				<label for="flie" class="col-sm-2 col-form-label">画像</label>
				<div class="col-sm-10">
					<input type="file" class="form-control" name="file" id="file">
				</div>
			</div>

			<div class="form-group row">
				<label for="money" class="col-sm-2 col-form-label">金額設定</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="price" id="money">
				</div>
			</div>

			<div class="submit-button-area">
				<button type="submit" value="登録"
					class="btn btn-dark btn-lg btn-block">登録</button>
			</div>
		</form>
		<div class="col-xs-4">
			<a href="Admin">戻る</a>
		</div>

	</div>
</body>
</html>