<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<h3>
		<p class="text-center">検索結果</p>

		<br> <br> <br>
	</h3>
	<div class="container">
		<br>

		<div class="btn-group">
			<button type="button" class="btn btn-success dropdown-toggle"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				並び替え</button>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="?${searchParam}&sort=new">新着順</a> <a
					class="dropdown-item" href="?${searchParam}&sort=old">古い順</a> <a
					class="dropdown-item" href="?${searchParam}&sort=cheaper">安い順</a> <a
					class="dropdown-item" href="?${searchParam}&sort=expensive">高い順</a>
			</div>
		</div>
		<br> <br>
		<div class="row">
			<c:forEach var="list" items="${list}">
				<div class="col s12 m3">
					<div class="card" style="width: 18rem;">
						<img class="card-img-top" src="img/${list.filename}"
							alt="画像">
						<div class="card-body">
							<h5 class="card-title">${list.name}</h5>
							<p class="card-text">${list.price}円</p>
							<a href="ItemDetail?item_id=${list.id}" class="btn btn-primary">詳細</a>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>