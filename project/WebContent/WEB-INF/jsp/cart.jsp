<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
<div class="container">
	<br> <br>
		<h3>
			<p class="text-center">
				<c:if test="${cartMsg!=null}">
			${cartMsg}</c:if>
			</p>
		</h3>
		<br> <br> <br>
		<div class="row center">
			<c:if test="${cartActionMessage!=null}" >
			${cartActionMessage}
			</c:if>
			<h3>買い物かご🧺</h3>
		</div>
		<div class="section">
			<form action="ItemDelete" method="POST">
				<div class="row">
					<div class="col s12">
						<div class="col s6 center-align">
							<button class="btn btn-warning col s6 offset-s3 " type="submit" name="action">
								削除<i class="material-icons right"></i>
							</button>
						</div>
						<div class="col s6 center-align">
							<a href="Buy" class="btn btn-info col s6 offset-s3">レジに進む<i class="material-icons right"></i></a>
						</div>
					</div>
				</div>
				<div class="row">
					<c:forEach var="item" items="${cart}" varStatus="status">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<img src="img/${item.filename}">
							</div>
							<div class="card-content">
								<span class="card-title">${item.name}</span>
								<p>${item.price}円
								</p>
								<p>
                                <input type="checkbox" id="${status.index}" name="delete_item_id_list" value="${item.id}" />
                                 <label for="${status.index}">削除</label>
								</p>
							</div>
						</div>
					</div>
					</c:forEach>
					</div>


</body>
</html>