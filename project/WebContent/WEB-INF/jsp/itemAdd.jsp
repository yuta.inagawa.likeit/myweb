<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css">
<title>INDEX</title>
</head>
<header>
	<div class="p-3 mb-2 bg-dark text-white" role="alert">
		<a href="Index">🧺STORE INAGAWA🏀</a>
		<p class="text-right">
			${user.user_name}さん <a href="Logout" class="badge badge-logout">ログアウトする</a> <a
				href="Cart"><button type="button" class="btn btn-light"><i class="fas fa-shopping-cart">カートへ</i></button></a>
			<a href="UserData"><button type="button"
					class="btn btn-light"><i class="fas fa-user">ユーザー情報</i></button></a>

					<c:if test="${user.login_id=='admin'}">
					<a href="Admin">
					<button type="button"
					class="btn btn-light"><i class="fas fa-user-secret">管理者画面</i></button></a>
					</c:if>
		</p>
	</div>
</header>
<body>
	<div class="container">
		<br> <br>
		<h3>
			<p class="text-center">
				<c:if test="${cartMsg!=null}">
			${cartMsg}</c:if>
			</p>
		</h3>
		<h3>
			<p class="text-center">買い物かご。</p>
		</h3>
		<br> <br> <br>


		<div class="row">
		<c:forEach var="items" items="${cart}">
			<div class="col s12 m3">
				<div class="card" style="width: 18rem;">
					<img class="card-img-top" src="img/${items.filename}" alt="カードの画像">
					<div class="card-body">
						<h5 class="card-title">${items.name}</h5>
						<p class="card-text">${items.price}円</p>
					</div>
				</div>
			</div>
		</c:forEach>
		</div>
		<p class="text-center">
			<a href="Index"><button type="button" class="btn btn-link">買い物を続ける🧺</a>
			</button>
		</p>
		<p class="text-center">
			<a href="Cart"><button type="button" class="btn btn-link">レジへ進む🛒</a>
			</button>
		</p>
			</div>
		</form>

	</div>
</body>
</html>