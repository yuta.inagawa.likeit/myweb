create table item (id int PRIMARY KEY auto_increment, name varchar(50) not null,detail varchar(150) not null,
price int not null,category_id int not null,player_id int,createdate date not null,filename varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

create table category_id(category_id int not null, name varchar(50));

create table user (id SERIAL PRIMARY KEY auto_increment,login_id varchar(50) unique not null, user_name varchar(50) not null, 
loginpass varchar(50)  not null, createdate date not null,address varchar(250) not null);

create table buy (id int primary key auto_increment,user_id varchar(50) not null,totalprice int not null,present_id int , 
create_date datetime not null);

create table present_id(id int not null,id_name varchar(50)not null,price int not null);

create table buy_detail(id int Primary Key auto_increment, buy_id int not null,item_id int not null);
